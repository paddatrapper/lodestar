FROM python:3

RUN apt-get update
RUN apt-get install -y -qq nginx poppler-utils

RUN rm /etc/nginx/sites-enabled/default

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip3 install uwsgi
RUN pip3 install cython
RUN pip3 install --no-cache-dir -r requirements.txt
RUN python3 -m nltk.downloader wordnet punkt

COPY docker/uwsgi.ini /etc/uwsgi/lodestar.ini
COPY docker/nginx.conf /etc/nginx/sites-enabled/lodestar.conf
COPY docker/start.sh /start.sh

COPY --chown=www-data:www-data ./LICENCE.md ./README.md ./manage.py ./
COPY --chown=www-data:www-data ./docs ./docs
COPY ./lodestar ./lodestar

RUN python3 manage.py migrate
RUN chown www-data:www-data ./db.sqlite3 ./manage.py .

EXPOSE 80
CMD ["/start.sh"]
