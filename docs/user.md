Available textbooks are listed as the home page for all users. Each textbook in
the list is a link to the textbooks content.

## Textbook content

Each textbook is displayed with keywords underlined in blue. These keywords have
definitions available that you can view by clicking on the word. The definition is hidden by clicking **Close** in the side panel that appears.
