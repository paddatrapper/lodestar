The **Site Admin** section of the website allows staff members to manage the
available ontologies and textbooks.

## Textbooks

Any available textbooks will be listed in this section. The **+ Add** Button
allows staff with the correct permissions to add new textbooks to the list.
Clicking on a textbook in the list will allow a member of staff to modify it.

### Adding a textbook

When adding a textbook, it must be linked with an existing
[ontology](#Ontologies). This means that new ontologies must be added first,
before any textbooks can make use of them. Textbooks can be uploaded as a single
HTML or PDF file. Once the textbook is added, the annotation is scheduled to
occur in the background. Thus, it takes a short time for the textbook to become
available to access from the [user section](user) of the website.

PDF files are converted into HTML, which strips them of any images, markup or
links. Only the plain text is retained. This conversion is not very accurate, so
it is recommended to upload an HTML file for any complex files.

## Ontologies

Any available ontologies will be listed in this section. The **+ Add** button allows staff with the correct permissions to add new ontologies to the list so
that they can be used in new textbooks.

### Adding an ontology

An ontology consists of one or more OWL (Web Ontology Language) files. These can
be uploaded as a zip archive if there are multiple files, or just as the OWL in
the single file case. The **Definition predicate** is the predicate (or
attribute) URI of classes that will be displayed as the class defintion. If
there are multiple predicates, the can be listed as a comma separated list with
no spaces. The last predicate found will be used.
