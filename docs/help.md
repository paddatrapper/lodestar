Lodestar is an digital smart textbook platform that offers keyword definitions
and related term listing based on an ontology. The site is broken up into two
sections:

* The [user section](docs/user)
* The [admin section](docs/admin)
