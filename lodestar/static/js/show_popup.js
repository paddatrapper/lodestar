/*
 * Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
 *
 * Lodestar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lodestar is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
 */
function showPopUp(popup_id) {
    var popup = $(document.getElementById(popup_id));
    var title = popup.prev().text();
    var relations = popup.next();
    console.log(relations.children());
    var text = popup.text();
    $('#class-col').show();
    $('#class-heading').text(title);
    $('#class-definition').text(text);
    var relationsList = $('#relationship-list');
    relationsList.empty();
    $.each(relations.children(), function(i) {
        var predicate = $(this).children().first().text();
        var obj = $(this).children().last().text();
        var li = $('<li/>')
            .text(predicate + ': ' + obj)
            .appendTo(relationsList);
    });
    var textbookCol = $('#textbook-col');
    textbookCol.addClass('col-sm-7');
    textbookCol.removeClass('col-sm-12');
}

$(document).ready(function() {
    $('#class-close-btn').click(function() {
        $('#class-col').hide();
        var textbookCol = $('#textbook-col');
        textbookCol.addClass('col-sm-12');
        textbookCol.removeClass('col-sm-7');
    });
});
