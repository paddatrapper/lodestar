###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import os
import shutil

from django.conf import settings
from django.core.files import File
from django.db.models.signals import post_save
from django.test import TestCase
from owlready2.annotation import comment

from lodestar.annotation.background import annotate_html, clean_term
from lodestar.renderer.models import Ontology, Textbook
from lodestar.sourceparser.models import Source
from lodestar.sourceparser.signals import convert_source
from lodestar.renderer.tests import get_textbook_file

class DefinitionAnnotationTest(TestCase):
    def test_annotate_html(self):
        post_save.disconnect(convert_source, Source)

        ontology_filename = 'testdata/DMKB.owl'
        ontology_fp = open(ontology_filename, 'rb')
        ontology = Ontology(name='Data Mining Optimized Ontology',
                            source_file=File(ontology_fp),
                            definition_predicate=comment.iri + ',http://www.e-lico.eu/ontologies/dmo/DMOP/DMOP.owl#definition',
                            primary_file='')
        ontology.save()
        ontology.primary_file = ontology.source_file
        ontology.save()
        shutil.copyfile('testdata/DMOP.owl',
                        os.path.join(os.path.dirname(ontology.primary_file.path), 'DMOP.owl'))

        fp = open('testdata/test_annotation_paragraph.html', 'rb')
        source = Source(name='Test html',
                        author='Kyle Robbertze',
                        source_file=File(fp))
        source.save()
        html_dir = os.path.join(settings.MEDIA_ROOT,
                                 'textbooks',
                                 'render',
                                 source.md5sum.hex())
        os.makedirs(html_dir, exist_ok=True)
        html_file = os.path.join(html_dir, 'content.html')
        shutil.copy(source.source_file.path, html_file)
        source.html_directory = html_dir
        source.save()
        post_save.connect(convert_source, Source)

        textbook = Textbook(ontology=ontology, source=source)
        textbook.save()
        annotate_html.now(textbook.pk)
        result_file = os.path.join(html_dir, 'index.html')
        result = ''
        with open(result_file) as result_fp:
            result = result_fp.readlines()
        result = ''.join(result)
        shutil.rmtree(ontology.get_root_path())
        shutil.rmtree(source.html_directory)
        os.remove(textbook.source.source_file.path)
        annotations = [
            '<span class="popup-text" id="definition-0">SSP stands for shortest spanning path (SSP) algorithm.</span>',
            '<span class="popup-text" id="definition-2">FishesLinearDiscriminantAlgorithm: is given by Bishop 06 as an example of Discriminant Functions as he defined them. Similarly Hand et al 01, p. 331,  cite it as a discriminative model (they merge Discriminative and Discriminant Function models).</span>',
            '<span class="popup-text" id="definition-3">DataSet: in data mining, the term data set is defined as a set of examples or instances represented according to a common schema.</span>',
            '<span class="relation-name">primaryReference</span><span class="relation-obj">SLAGLE, J. R., CHANG, C. L., AND HELLER, S. R. 1975. A clustering and data-reorganizing algorithm. IEEE Trans. Syst. Man Cybern. 5, 125–128</span>',
        ]
        for annotation in annotations:
            self.assertInHTML(annotation, result)

        bad_annotations = [
            '<span class="popup-text" id="definition-3">Being a (generic, temporary) constituent in a countable collection, for example: member of a society, bacterium in a colony, etc.</span>',
            '<span class="relation-name">comment</span><span class="relation-obj">SSP stands for shortest spanning path (SSP) algorithm.</span>',
        ]
        for bad_annotation in bad_annotations:
            self.assertNotIn(bad_annotation, result)

    def test_clean_term(self):
        uri = 'https://example.org/test.owl#Test'
        result = clean_term(uri)
        self.assertEqual(result, 'Test')

    def test_clean_term_url(self):
        uri = 'https://example.org/test.owl/Test'
        result = clean_term(uri)
        self.assertEqual(result, 'Test')

    def test_clean_term_dot(self):
        term = 'onto.Test'
        result = clean_term(term)
        self.assertEqual(result, 'Test')

    def test_clean_term_bool(self):
        result = clean_term(True)
        self.assertEqual(result, 'True')
