###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import re

from itertools import chain
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk.corpus import wordnet

"""
Matching functions are processed in the oder they appear in the list. They must
accept two arguments:
    term: the term in the ontology
    line: the line in the textbook

They must also return a dictionary containing
    stop: True if the matcher should stop searching
    match: the regular expression matching the text as it appears in the
           textbook. This is suitable for placing in a regular expression
           substitution and expecting it to replace the term completely.
    match_term: the matching, human readable, text as it appears in the
                textbook
"""
MATCHERS = []
REGEX = r'\b{:s}\b(?![\s\w\d()\./:;\'"=&?,\-\–\[\]]*?<\/span>)'

def has_match(term, line):
    """
    Tests if a term occurs in a line of text. This is limited to single words
    at the moment and is case sensitive.

    Args:
        term: the term in the ontology
        line: the line in the textbook

    Returns:
        match: the regular expression matching the text as it appears in the
               textbook. This is suitable for placing in a regular expression
               substitution and expecting it to replace the term completely.
        match_term: the matching, human readable, text as it appears in the
                    textbook
    """
    for matcher in MATCHERS:
        result = matcher(term, line)
        if result['stop']:
            return result['match'], result['match_term']
    return None, None

def run_single_word_matchers(term, line):
    """
    Runs all matchers that expect to match against a single word. This should
    never be added to MATCHERS, as the functions it runs are already appended in
    their correct positions.
    """
    functions = [match_word_case_insensitive, match_plural_of]
    for f in functions:
        result = f(term, line)
        if result['stop']:
            return result
    return {'stop': False}

def match_numeric(term, line):
    # Single number are generic and can have many meanings
    if term.isnumeric():
        return {'stop': True, 'match': None, 'match_term': None}
    return {'stop': False}

def match_acronym(term, line):
    # Most likely an acronym that should not be matched against phrases
    if term.isupper():
        return {'stop': True, 'match': None, 'match_term': None}
    return {'stop': False}

def match_phrase_case_insensitive(term, line):
    # Title case and lower case phrases
    phrase = re.sub(r'[A-Z]', lambda m:' ' + m.group(0), term).strip()
    for phrase in [phrase, phrase.lower()]:
        if phrase == term.lower():
            continue
        match = REGEX.format(phrase)
        if re.search(match, line):
            return {'stop': True, 'match': match, 'match_term': phrase}
    return {'stop': False}

def match_word_case_insensitive(term, line):
    # Match against a word, all lower case or Title case
    for t in [term, term.lower(), term.title()]:
        match = REGEX.format(t)
        if re.search(match, line):
            return {'stop': True, 'match': match, 'match_term': t}
    return {'stop': False}

def match_plural_of(term, line):
    wnl = WordNetLemmatizer()
    # Break up line into words
    line_list = word_tokenize(line)
    # Generate list of singular versions of each word
    line_list_single = [wnl.lemmatize(x.lower()) for x in line_list]
    if term.lower() in line_list_single:
        for i in range(len(line_list)):
            if line_list_single[i].lower() == term.lower():
                # Matched a plural version of the term
                return {'stop': True,
                        'match': REGEX.format(line_list[i]),
                        'match_term': line_list[i]}
    return {'stop': False}

def match_prefixed_term(term, line):
    # Terms with a prefix making them unique to the domain
    term_comp = term.split('-')
    if len(term_comp) == 2 and term_comp[0].isupper():
        term = term_comp[1]
        return run_single_word_matchers(term, line)
    return {'stop': False}

def match_synonyms_of(term, line):
    # Only look for synonyms if there are no direct matches for the word or
    # phrase
    synonym_set = wordnet.synsets(term)
    synonyms = set(chain.from_iterable([word.lemma_names() for word in synonym_set]))
    for synonym in synonyms:
            result = run_single_word_matchers(synonym, line)
            if result['stop']:
                return result
    return {'stop': False}

MATCHERS.append(match_numeric)
#MATCHERS.append(match_acronym)
MATCHERS.append(match_phrase_case_insensitive)
MATCHERS.append(match_word_case_insensitive)
MATCHERS.append(match_plural_of)
MATCHERS.append(match_prefixed_term)
#MATCHERS.append(match_synonyms_of)
