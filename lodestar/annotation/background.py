###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import os
import operator
import re
import owlready2
import shutil
import string

from collections import OrderedDict
from background_task import background
from django.db.models import Model
from django.utils.translation import gettext_lazy as _

from lodestar.renderer.models import Textbook
from .matching import has_match

def clean_term(term):
    """
    Cleans a term to just return the class name

    Args:
        term: the owlready2 individual or uri to clean. This is in the format
              <onto_short_name>.<name> or uri using # or / to deliminate the
              term from the rest of the string

    Returns:
        The name of the term
    """
    term = str(term)
    if '#' in term:
        term = term.split('#')[-1]
    if '/' in term:
        term = term.split('/')[-1]
    if '.' in term:
        return term.split('.')[-1]
    return term

def get_item(item, exclude=[]):
    """
    Generates the format string that is used to insert details as HTML elements
    in the textbook

    Args:
        item: the ontology individual or class that will be used to find
                        relationships
        exclude: a list of relationships to ignore. This is useful for ignoring
                 things like the definition predicate. Default is an empty list

    Returns:
        A formatted string Template with the keywords:
            i - indicating the term counter - this is unique to the popup
            term - the term in the ontology
            definition - the definition of the term
    """
    format_string = '<span class="popup" onclick="showPopUp(\'definition-${i}\')"><span id="class-${i}">${term}</span><span class="popup-text" id="definition-${i}">${definition}</span><span class="popup-relations" id="relations-${i}">##relations##</span></span>'
    relations_string = '<span class="popup-relation" id="relation-{i:d}"><span class="relation-name">{relation}</span><span class="relation-obj">{obj}</span></span>'
    relations = []
    counter = 0
    try:
        props = item.get_properties()
    except TypeError:
        # Classes need an individual created of which to get the properties
        props = item().get_properties()
    except AttributeError:
        # Relationships have no relationships
        return string.Template(format_string.replace('##relations##', ''))

    for p in props:
        if p in exclude:
            continue
        relationship = p.python_name
        objs = p[item]
        for obj in objs:
            obj = str(obj).replace('\\', '\\\\')
            relations.append(relations_string.format(i=counter,
                                                     relation=relationship,
                                                     obj=obj))
            counter = counter + 1
    for cls in item.is_a:
        relations.append(relations_string.format(i=counter,
                                                 relation=_('type'),
                                                 obj=cls))
        counter = counter + 1
    relationship_list = ''.join(relations)
    return string.Template(format_string.replace('##relations##',
                                                 relationship_list))

def sort_map_item(kv):
    """
    Returns the comparison value for the key, value pair. This value is the
    number of capital letters exist without a '-' or additional capital letter
    proceeding them in the key.

    Args:
        kv: the key-value pair

    Returns:
        The number of capital letters in the key
    """
    key = str(kv[0])
    matches = re.findall(r'-[A-Z]|[A-Z](?![A-Z]|-)', key)
    return len(matches)

@background(schedule=1)
def annotate_html(textbook_id):
    """
    Links terms in a textbook to the definitions in the ontology. The
    textbook_id is required to allow the function call to be serialised by
    background_task. The annotation is applied directly to the html and embedded
    into the output html file. This reduces computation on each request, but
    requires the html to be regenerated and the annotations to be re-applied
    each time the ontology or textbook are changed.

    Args:
        textbook_id: the id of the textbook to annotate
    """
    try:
        textbook = Textbook.objects.get(id=textbook_id)
    except Textbook.DoesNotExist:
        # Textbook has been deleted since the annotation was scheduled
        return
    print('Annotating textbook {}'.format(textbook))
    ontology = textbook.ontology
    file_form = 'file://{:s}'
    uri = file_form.format(ontology.primary_file.path)
    owlready2.onto_path.append(os.path.dirname(ontology.primary_file.path))
    g = owlready2.get_ontology(uri).load()

    predicates = [g.search_one(iri=x) for x in ontology.definition_predicate.split(',')]
    map_dict = {}
    for predicate in predicates:
        if predicate is None:
            continue
        term = predicate.python_name
        attributes = { term: '*' }
        for i in g.search(**attributes):
            map_dict[i] = getattr(i, term).first().strip().replace('\\', '\\\\')
    for individual in g.individuals():
        if individual in map_dict:
            # Already exists
            continue
        # No definition, but could have related concepts
        map_dict[individual] = ''

    sorted_map = sorted(map_dict.items(), key=sort_map_item, reverse=True)
    map_dict = OrderedDict(sorted_map)
    html_file = os.path.join(textbook.source.html_directory, 'content.html')
    new_file = os.path.join(textbook.source.html_directory, 'index.html')
    with open(html_file) as fp, open(new_file, 'w') as dest:
        counter = 0;
        for line in fp:
            for subject, definition in map_dict.items():
                term = clean_term(subject)
                match, match_term = has_match(term, line)
                if match is None:
                    continue
                item_format = get_item(subject, predicates)
                replacement = item_format.safe_substitute(term=match_term,
                                                          definition=definition,
                                                          i=counter)
                old_line = line
                line = re.sub(match, replacement, line)
                if line != old_line:
                    counter = counter + 1
            dest.write(line)
    print('Done')
