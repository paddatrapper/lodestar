###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import os

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.urls import reverse

from lodestar import get_file_hash
from lodestar.sourceparser.models import Source

class Ontology(models.Model):
    def upload_source(instance, filename):
        file_hash = get_file_hash(instance.source_file.file).hex()
        return os.path.join('ontology', file_hash, filename)

    def upload_primary(instance, filename):
        file_hash = get_file_hash(instance.source_file.file).hex()
        return os.path.join('ontology', file_hash, filename)

    name = models.CharField(_('name'), max_length=200)
    source_file = models.FileField(upload_to=upload_source, max_length=130)
    md5sum = models.BinaryField(max_length=128, blank=True)
    definition_predicate = models.CharField(_('defintion predicate'),
                                              max_length=200)
    primary_file = models.FileField(upload_to=upload_primary, null=True, max_length=130)

    def get_absolute_url(self):
        return reverse('renderer:admin_edit_ontology', kwargs={'pk': self.pk})

    def get_root_path(self):
        return os.path.join(settings.MEDIA_ROOT, 'ontology', self.md5sum.hex())

    def __str__(self):
        return self.name

class Textbook(models.Model):
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    ontology = models.ForeignKey(Ontology, on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('renderer:admin_edit_textbook', kwargs={'pk': self.pk})

    def get_absolute_view_url(self):
        return reverse('renderer:view_textbook', kwargs={'pk': self.pk})

    def __str__(self):
        return '{} by {}'.format(self.source.name, self.source.author)
