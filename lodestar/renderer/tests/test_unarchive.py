###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import os
import shutil

from django.test import TestCase

from lodestar.renderer.unarchive import unzip, is_archive, unarchive

class UnarchiveTest(TestCase):
    def test_unzip(self):
        filename = os.path.join('testdata', 'ontology.zip')
        destination = os.path.join('testdata', 'test_run')
        namelist = unzip(filename, destination)
        shutil.rmtree(destination)
        self.assertSequenceEqual(namelist, ['DMKB.owl', 'DMOP.owl', 'PD.owl'])

    def test_is_archive_true(self):
        filename = os.path.join('testdata', 'ontology.zip')
        self.assertTrue(is_archive(filename))

    def test_is_archive_false(self):
        filename = os.path.join('testdata', 'DMKB.owl')
        self.assertFalse(is_archive(filename))

    def test_unarchive_zip(self):
        filename = os.path.join('testdata', 'ontology.zip')
        destination = os.path.join('testdata', 'test_run')
        namelist = unarchive(filename, destination)
        shutil.rmtree(destination)
        self.assertSequenceEqual(namelist, ['DMKB.owl', 'DMOP.owl', 'PD.owl'])
