###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import os
import shutil

from django.contrib.auth.models import AnonymousUser, User, Permission, ContentType
from django.core.exceptions import PermissionDenied
from django.core.files import File
from django.test import RequestFactory, TestCase, Client
from django.urls import reverse

from lodestar.renderer.admin_views import index, add_textbook, OntologyCreate, OntologyUpdate, OntologyDelete, set_ontology_primary_file, edit_textbook
from lodestar.renderer.models import Ontology, Textbook

from . import get_ontology_file, get_textbook_file

class AdminIndexViewTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.path = reverse('renderer:admin_index')
        cls.factory = RequestFactory()
        cls.request = cls.factory.get(cls.path)
        cls.user = User.objects.create_user(username='jacob', is_staff=True)

    def test_not_logged_in(self):
        self.request.user = AnonymousUser()
        response = index(self.request)
        self.assertEqual(response.status_code, 302)

    def test_logged_in_no_permission(self):
        user = User.objects.create_user(username='user')
        self.request.user = user
        response = index(self.request)
        self.assertEqual(response.status_code, 302)

    def test_logged_in(self):
        self.request.user = self.user
        response = index(self.request)
        self.assertContains(response, 'You have no textbooks')
        self.assertContains(response, 'You have no ontologies')
        self.assertNotContains(response, '<div class="alert alert-success alert-dismissible fade show" role="alert"> None <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div>', html=True)

    def test_logged_in_cookie(self):
        factory = RequestFactory()
        factory.cookies['message'] = 'This is a test message'
        request = factory.get(self.path)
        request.user = self.user
        response = index(request)
        self.assertContains(response, 'This is a test message')


class AdminAddOntologyTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.path = reverse('renderer:admin_add_ontology')
        cls.factory = RequestFactory()
        cls.request = cls.factory.get(cls.path)
        cls.content_type = ContentType.objects.get(app_label='renderer',
                                                   model='ontology')

    def test_unauthorized(self):
        user = User.objects.create_user(username='jacob')
        self.request.user = user
        self.assertRaises(PermissionDenied, OntologyCreate.as_view(),
                          self.request)

    def test_authorized(self):
        user = User.objects.create_user(username='jacob')
        add_permission = Permission.objects.get(codename='add_ontology',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        self.request.user = user
        response = OntologyCreate.as_view()(self.request)
        self.assertContains(response, 'Add ontology')
        self.assertContains(response, 'Save')

    def test_successful_non_zip(self):
        user = User.objects.create_user(username='jacob')
        add_permission = Permission.objects.get(codename='add_ontology',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        self.request.user = user
        filename, ontology = get_ontology_file()
        source = open('testdata/DMOP.owl', 'rb')
        ontology.source_file = File(source)
        create_view = OntologyCreate(request=self.request)
        form = create_view.get_form()
        form.instance.source_file = ontology.source_file
        form.cleaned_data = {
            'name':  ontology.name,
            'definition_predicate': ontology.definition_predicate,
        }
        response = create_view.form_valid(form)
        shutil.rmtree(ontology.get_root_path())
        self.assertRedirects(response, create_view.get_success_url(),
                             fetch_redirect_response=False)

    def test_successful_zip(self):
        user = User.objects.create_user(username='jacob')
        add_permission = Permission.objects.get(codename='add_ontology',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        self.request.user = user
        filename, ontology = get_ontology_file()
        create_view = OntologyCreate(request=self.request)
        form = create_view.get_form()
        form.cleaned_data = {
            'name':  ontology.name,
            'definition_predicate': ontology.definition_predicate,
        }
        form.instance.source_file = ontology.source_file
        response = create_view.form_valid(form)
        shutil.rmtree(ontology.get_root_path())
        self.assertRedirects(response,
                             reverse('renderer:admin_edit_ontology_primary_file',
                                     args=[form.instance.pk]),
                             fetch_redirect_response=False)

class AdminUpdateOntologyTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = RequestFactory()
        cls.content_type = ContentType.objects.get(app_label='renderer',
                                                   model='ontology')

    def test_unauthorized(self):
        filename, ontology = get_ontology_file()
        ontology.save()
        path = reverse('renderer:admin_edit_ontology', args=[ontology.pk])
        request = self.factory.get(path)
        user = User.objects.create_user(username='jacob')
        request.user = user
        shutil.rmtree(ontology.get_root_path())
        self.assertRaises(PermissionDenied, OntologyUpdate.as_view(),
                          request, ontology.pk)

    def test_authorized(self):
        filename, ontology = get_ontology_file()
        ontology.save()
        path = reverse('renderer:admin_edit_ontology', args=[ontology.pk])
        request = self.factory.get(path)
        user = User.objects.create_user(username='jacob')
        add_permission = Permission.objects.get(codename='change_ontology',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        request.user = user
        response = OntologyUpdate.as_view()(request, pk=ontology.pk)
        shutil.rmtree(ontology.get_root_path())
        self.assertContains(response, 'Update ontology')
        self.assertContains(response, 'Save')

class AdminDeleteOntologyTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = RequestFactory()
        cls.content_type = ContentType.objects.get(app_label='renderer',
                                                   model='ontology')

    def test_unauthorized(self):
        filename, ontology = get_ontology_file()
        ontology.save()
        path = reverse('renderer:admin_delete_ontology', args=[ontology.pk])
        request = self.factory.get(path)
        user = User.objects.create_user(username='jacob')
        request.user = user
        shutil.rmtree(ontology.get_root_path())
        self.assertRaises(PermissionDenied, OntologyDelete.as_view(),
                          request, ontology.pk)

    def test_authorized(self):
        filename, ontology = get_ontology_file()
        ontology.save()
        path = reverse('renderer:admin_delete_ontology', args=[ontology.pk])
        request = self.factory.get(path)
        user = User.objects.create_user(username='jacob')
        add_permission = Permission.objects.get(codename='delete_ontology',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        request.user = user
        response = OntologyDelete.as_view()(request, pk=ontology.pk)
        shutil.rmtree(ontology.get_root_path())
        self.assertContains(response, 'Delete ontology')

class AdminSetOntologyPrimaryFileTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = RequestFactory()
        cls.content_type = ContentType.objects.get(app_label='renderer',
                                                   model='ontology')

    def test_unauthorized(self):
        filename, ontology = get_ontology_file()
        ontology.save()
        path = reverse('renderer:admin_edit_ontology_primary_file',
                       args=[ontology.pk])
        request = self.factory.get(path)
        user = User.objects.create_user(username='jacob')
        request.user = user
        response = set_ontology_primary_file(request, ontology.pk)
        shutil.rmtree(ontology.get_root_path())
        self.assertEqual(response.status_code, 302)

    def test_authorized_archive(self):
        filename, ontology = get_ontology_file()
        ontology.save()
        path = reverse('renderer:admin_edit_ontology_primary_file',
                       args=[ontology.pk])
        request = self.factory.get(path)
        user = User.objects.create_user(username='jacob')
        add_permission = Permission.objects.get(codename='change_ontology',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        request.user = user
        response = set_ontology_primary_file(request, ontology.pk)
        shutil.rmtree(ontology.get_root_path())
        self.assertContains(response, 'Set Primary File')
        self.assertContains(response, 'Save')

    def test_not_archive(self):
        filename, ontology = get_ontology_file()
        source = open('testdata/DMOP.owl', 'rb')
        ontology.source_file = File(source)
        ontology.save()
        path = reverse('renderer:admin_edit_ontology_primary_file',
                       args=[ontology.pk])
        request = self.factory.get(path)
        user = User.objects.create_user(username='jacob', is_staff=True)
        add_permission = Permission.objects.get(codename='change_ontology',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        request.user = user
        response = set_ontology_primary_file(request, ontology.pk)
        shutil.rmtree(ontology.get_root_path())
        ontology.refresh_from_db()
        self.assertEqual(ontology.source_file, ontology.primary_file)
        self.assertEqual(response.cookies.get('message').value,
                         '{} has been updated successfully'.format(ontology))

    def test_authorized_archive_post(self):
        filename, ontology = get_ontology_file()
        ontology.save()
        path = reverse('renderer:admin_edit_ontology_primary_file',
                       args=[ontology.pk])
        user = User.objects.create_user(username='jacob', is_staff=True)
        add_permission = Permission.objects.get(codename='change_ontology',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        self.client.force_login(user)
        response = self.client.post(path, {'primary_file': 'DMKB.owl'})
        self.assertEqual(response.cookies.get('message').value,
                         '{} has been updated successfully'.format(ontology))
        saved_ontology = Ontology.objects.get(id=ontology.pk)
        file_path = os.path.join(ontology.get_root_path(), 'DMKB.owl')
        # Deal with random extension added to path by Django storage
        self.assertIn(file_path[:-4], saved_ontology.primary_file.name)
        shutil.rmtree(ontology.get_root_path())

class AdminAddTextbookTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = RequestFactory()
        cls.content_type = ContentType.objects.get(app_label='renderer',
                                                   model='textbook')
        cls.path = reverse('renderer:admin_add_textbook')

    def test_unauthorized(self):
        filename, ontology = get_ontology_file()
        ontology.save()
        request = self.factory.get(self.path)
        user = User.objects.create_user(username='jacob')
        request.user = user
        response = add_textbook(request)
        shutil.rmtree(ontology.get_root_path())
        self.assertEqual(response.status_code, 302)

    def test_authorized(self):
        filename, ontology = get_ontology_file()
        ontology.save()
        request = self.factory.get(self.path)
        user = User.objects.create_user(username='jacob')
        add_permission = Permission.objects.get(codename='add_textbook',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        request.user = user
        response = add_textbook(request)
        shutil.rmtree(ontology.get_root_path())
        self.assertContains(response, 'Add Textbook')
        self.assertContains(response, 'Save')

    def test_authorized_post(self):
        filename, ontology = get_ontology_file()
        ontology.save()
        user = User.objects.create_user(username='jacob', is_staff=True)
        add_permission = Permission.objects.get(codename='add_textbook',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        self.client.force_login(user)
        response = None
        with open('testdata/DataMiningForTheMasses.pdf', 'rb') as fp:
            response = self.client.post(self.path, {'name': 'Test Textbook',
                                                    'author': 'Test author',
                                                    'source_file': fp,
                                                    'ontology': ontology.pk})
        shutil.rmtree(ontology.get_root_path())
        textbook = ontology.textbook_set.all()[0]
        self.assertEqual(response.cookies.get('message').value,
                         '{} has been added successfully'.format(textbook))
        self.assertIsNotNone(textbook)
        self.assertTrue(os.path.isfile(os.path.join(textbook.source.html_directory, 'content.html')))
        shutil.rmtree(textbook.source.html_directory)

    def test_authorized_post_missing_file(self):
        filename, ontology = get_ontology_file()
        ontology.save()
        user = User.objects.create_user(username='jacob', is_staff=True)
        add_permission = Permission.objects.get(codename='add_textbook',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        self.client.force_login(user)
        response = None
        with open('testdata/DataMiningForTheMasses.pdf', 'rb') as fp:
            response = self.client.post(self.path, {'name': 'Test Textbook',
                                                    'author': 'Test author',
                                                    'ontology': ontology.pk})
        shutil.rmtree(ontology.get_root_path())
        self.assertFormError(response, 'form', 'source_file',
                             'This field is required')

class AdminEditTextbookTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.factory = RequestFactory()
        cls.content_type = ContentType.objects.get(app_label='renderer',
                                                   model='textbook')

    def test_unauthorized(self):
        textbook = get_textbook_file()
        textbook.save()
        path = reverse('renderer:admin_edit_textbook', args=[textbook.pk])
        request = self.factory.get(path)
        user = User.objects.create_user(username='jacob')
        request.user = user
        response = edit_textbook(request, textbook.pk)
        shutil.rmtree(textbook.ontology.get_root_path())
        shutil.rmtree(textbook.source.html_directory)
        os.remove(textbook.source.source_file.path)
        self.assertEqual(response.status_code, 302)

    def test_authorized(self):
        textbook = get_textbook_file()
        textbook.save()
        path = reverse('renderer:admin_edit_textbook', args=[textbook.pk])
        request = self.factory.get(path)
        user = User.objects.create_user(username='jacob')
        add_permission = Permission.objects.get(codename='change_textbook',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        request.user = user
        response = edit_textbook(request, textbook.pk)
        shutil.rmtree(textbook.ontology.get_root_path())
        shutil.rmtree(textbook.source.html_directory)
        os.remove(textbook.source.source_file.path)
        self.assertContains(response, 'Edit Textbook')
        self.assertContains(response, 'Save')

    def test_authorized_post(self):
        textbook = get_textbook_file()
        textbook.save()
        path = reverse('renderer:admin_edit_textbook', args=[textbook.pk])
        user = User.objects.create_user(username='jacob', is_staff=True)
        add_permission = Permission.objects.get(codename='change_textbook',
                                                content_type=self.content_type)
        user.user_permissions.add(add_permission)
        self.client.force_login(user)
        response = None
        with open('testdata/DataMiningForTheMasses.pdf', 'rb') as fp:
            response = self.client.post(path,
                                        {'name': 'New Textbook',
                                         'author': 'Test author',
                                         'source_file': fp,
                                         'ontology': textbook.ontology.pk})
        shutil.rmtree(textbook.ontology.get_root_path())
        self.assertTrue(os.path.isfile(os.path.join(textbook.source.html_directory, 'content.html')))
        shutil.rmtree(textbook.source.html_directory)
        os.remove(textbook.source.source_file.path)
        textbook.refresh_from_db()
        self.assertEqual(response.cookies.get('message').value,
                         '{} has been updated successfully'.format(textbook))
        self.assertEquals(textbook.source.name, 'New Textbook')
