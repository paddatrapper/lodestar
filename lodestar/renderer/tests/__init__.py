###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from owlready2.annotation import comment
from lodestar.renderer.models import Ontology, Textbook

from django.core.files import File

from lodestar.sourceparser.tests import get_source_file

def get_ontology_file(name='Data Mining Optimized Ontology'):
    filename = 'testdata/ontology.zip'
    fp = open(filename, 'rb')
    ontology = Ontology(name=name,
                        source_file=File(fp),
                        definition_predicate=comment.iri,
                        primary_file=File(fp))
    return filename, ontology

def get_textbook_file():
    ontology_fp, ontology = get_ontology_file()
    ontology.save()
    source_fp, source = get_source_file()
    source.save()
    textbook = Textbook(source=source, ontology=ontology)
    return textbook
