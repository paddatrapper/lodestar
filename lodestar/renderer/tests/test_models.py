###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import hashlib
import os
import shutil

from django.core.files import File
from django.db.models.signals import post_save
from django.test import TestCase

from lodestar.renderer.models import Ontology
from lodestar.renderer.signals import handle_calculate_hash
from . import get_ontology_file, get_textbook_file

BUFFER_SIZE = 65536 # 64kb

def get_file_hash(filename):
    md5 = hashlib.md5()
    with open(filename, 'rb') as f:
        while True:
            data = f.read(BUFFER_SIZE)
            if not data:
                break;
            md5.update(data)
    return md5.digest()

class OntologyTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.filename, cls.ontology = get_ontology_file()

    def test_create_signals(self):
        expected_hash = get_file_hash(self.filename)
        self.assertEqual(self.ontology.md5sum, b'')
        handle_calculate_hash(Ontology, self.ontology, None)
        self.assertEqual(self.ontology.md5sum, expected_hash)

    def test_to_string(self):
        filename, ontology = get_ontology_file('Test file')
        self.assertEqual(ontology.__str__(), 'Test file')
