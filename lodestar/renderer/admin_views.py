###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import os

from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.decorators import user_passes_test, permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import ContentType
from django.core.files import File
from django.conf import settings
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import gettext_lazy as _
from django.urls import reverse_lazy, reverse
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic import ListView

from lodestar.sourceparser.models import Source
from lodestar.annotation.background import annotate_html

from .forms import PrimaryFileForm, TextbookForm, TextbookUpdateForm
from .models import Textbook, Ontology
from .unarchive import is_archive, unarchive

class OntologyCreate(PermissionRequiredMixin, CreateView):
    permission_required = 'renderer.add_ontology'
    template_name = 'renderer/admin/add_update_object.html'
    model = Ontology
    success_url = reverse_lazy('renderer:admin_index')
    fields = ['name', 'source_file', 'definition_predicate']
    extra_context = {'title': _('Add ontology'),
                     'form_url': reverse_lazy('renderer:admin_add_ontology'),}

    def form_valid(self, form):
        instance = form.instance
        if not is_archive(instance.source_file.path):
            instance.primary_file = instance.source_file
        response = super().form_valid(form)
        if is_archive(instance.source_file.path):
            url = reverse_lazy('renderer:admin_edit_ontology_primary_file',
                               args=[instance.pk])
            response = redirect(url)
        content_type = ContentType.objects.get(app_label='renderer',
                                               model='ontology')
        LogEntry.objects.log_action(self.request.user.pk,
                                    content_type.pk,
                                    instance.pk,
                                    str(instance),
                                    ADDITION)
        return response

class OntologyUpdate(PermissionRequiredMixin, UpdateView):
    permission_required = 'renderer.change_ontology'
    template_name = 'renderer/admin/add_update_object.html'
    model = Ontology
    success_url = reverse_lazy('renderer:admin_index')
    fields = ['name', 'source_file', 'definition_predicate']
    extra_context = {'title': _('Update ontology')}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_url'] = reverse_lazy('renderer:admin_edit_ontology',
                                           args=[context['object'].id])
        context['delete_url'] = reverse_lazy('renderer:admin_delete_ontology',
                                             args=[context['object'].id])
        return context

    def form_valid(self, form):
        instance = form.instance
        if not is_archive(instance.source_file.path):
            instance.primary_file = instance.source_file
        response = super().form_valid(form)
        if is_archive(instance.source_file.path) and 'source_file' in form.changed_data:
            url = reverse_lazy('renderer:admin_edit_ontology_primary_file',
                               args=[instance.pk])
            response = redirect(url)
        instance.save()
        if form.has_changed():
            content_type = ContentType.objects.get(app_label='renderer',
                                                   model='ontology')
            LogEntry.objects.log_action(self.request.user.pk,
                                        content_type.pk,
                                        instance.pk,
                                        str(instance),
                                        CHANGE,
                                        form.changed_data)
        return response

class OntologyDelete(PermissionRequiredMixin, DeleteView):
    permission_required = 'renderer.delete_ontology'
    template_name = 'renderer/admin/delete_confirmation.html'
    model = Ontology
    success_url = reverse_lazy('renderer:admin_index')
    extra_context = {'title': _('Delete ontology')}

    def form_valid(self, form):
        instance = form.instance
        content_type = ContentType.objects.get(app_label='renderer',
                                               model='ontology'),
        LogEntry.objects.log_action(self.request.user.pk,
                                    content_type.pk,
                                    instance.pk,
                                    str(instance),
                                    DELETION)
        return super().form_valid(form)

def is_staff_check(user):
    return user.is_staff

@user_passes_test(is_staff_check)
def index(request):
    message = request.COOKIES.get('message')
    textbooks = Textbook.objects.all()
    ontologies = Ontology.objects.all()
    context = {
        'textbooks': textbooks,
        'ontologies': ontologies,
    }
    if request.user.has_perm('auth.add_user'):
        context['users'] = True
    if message:
        context['messages'] = [message,]
    response = render(request, 'renderer/admin/index.html', context)
    if message:
        response.delete_cookie('message')
    return response

@permission_required('renderer.add_textbook')
def add_textbook(request):
    if request.method == 'POST':
        form = TextbookForm(request.POST, request.FILES)
        if form.is_valid():
            source = Source(name=form.cleaned_data['name'],
                            author=form.cleaned_data['author'],
                            source_file=form.cleaned_data['source_file'])
            source.save()
            ontology = form.cleaned_data['ontology']
            textbook = Textbook(source=source,
                                ontology=ontology)
            textbook.save()
            content_type = ContentType.objects.get(app_label='renderer',
                                                   model='textbook')
            LogEntry.objects.log_action(request.user.pk,
                                        content_type.pk,
                                        textbook.pk,
                                        str(textbook),
                                        ADDITION)
            response = redirect('renderer:admin_index')
            response.set_cookie('message',
                                _('{obj} has been added successfully').format(obj=textbook))
            return response
    else:
        form = TextbookForm()

    context = {
        'form': form,
        'title': _('Add Textbook'),
        'form_url': reverse('renderer:admin_add_textbook'),
    }
    return render(request, 'renderer/admin/add_update_object.html', context)

@permission_required('renderer.change_textbook')
def edit_textbook(request, pk):
    textbook = get_object_or_404(Textbook, id=pk)
    source = textbook.source
    ontology = textbook.ontology
    initial = {
        'name': source.name,
        'author': source.author,
        'ontology': ontology.pk,
        'source_file': source.source_file,
    }
    if request.method == 'POST':
        form = TextbookUpdateForm(request.POST, request.FILES,
                            require_file=False, initial=initial)
        if form.is_valid():
            source.name = form.cleaned_data['name']
            source.author = form.cleaned_data['author']
            if form.cleaned_data['source_file']:
                source.source_file = form.cleaned_data['source_file']
            source.save()
            textbook.ontology = form.cleaned_data['ontology']
            textbook.save()
            additional_message = ''
            if form.cleaned_data['reprocess_textbook']:
                annotate_html(textbook.pk)
                additional_message = _(' and scheduled for re-processing')
            if form.has_changed():
                content_type = ContentType.objects.get(app_label='renderer',
                                                       model='textbook')
                LogEntry.objects.log_action(request.user.pk,
                                            content_type.pk,
                                            textbook.pk,
                                            str(textbook),
                                            CHANGE,
                                            form.changed_data)
            response = redirect('renderer:admin_index')
            response.set_cookie('message',
                                _('{obj} has been updated successfully{extra}'.format(obj=textbook, extra=additional_message)))
            return response
    else:
        form = TextbookUpdateForm(initial=initial, require_file=False)

    context = {
        'form': form,
        'title': _('Edit Textbook'),
        'form_url': reverse('renderer:admin_edit_textbook', args=[textbook.pk]),
        'delete_url': reverse_lazy('renderer:admin_delete_textbook',
                                   args=[textbook.id]),
    }
    return render(request, 'renderer/admin/add_update_object.html', context)

@permission_required('renderer:delete_textbook')
def delete_textbook(request, pk):
    textbook = get_object_or_404(Textbook, id=pk)
    if request.method == 'POST':
        textbook.delete()
        content_type = ContentType.objects.get(app_label='renderer',
                                               model='textbook')
        LogEntry.objects.log_action(request.user.pk,
                                    content_type.pk,
                                    textbook.pk,
                                    str(textbook),
                                    DELETION)
        response = redirect('renderer:admin_index')
        response.set_cookie('message',
                            _('{obj} has been deleted successfully').format(obj=textbook))
        return response

    context = {
        'object': textbook,
        'title': _('Delete textbook'),
    }
    return render(request, 'renderer/admin/delete_confirmation.html', context)

@permission_required('renderer.change_ontology')
def set_ontology_primary_file(request, pk):
    ontology = get_object_or_404(Ontology, id=pk)
    source = ontology.source_file
    if not is_archive(source.path):
        ontology.primary_file = source
        ontology.save()
        response = redirect('renderer:admin_index')
        response.set_cookie('message',
                            _('{obj} has been updated successfully').format(obj=ontology))
        return response
    files = unarchive(source.path, ontology.get_root_path())
    files = [(f, f) for f in files]
    if request.method == 'POST':
        form = PrimaryFileForm(request.POST, file_list=files)
        if form.is_valid():
            primary_file = os.path.join(ontology.get_root_path(),
                                        form.cleaned_data['primary_file'])
            ontology.primary_file = File(open(primary_file))
            ontology.save()
            response = redirect('renderer:admin_index')
            response.set_cookie('message',
                                _('{obj} has been updated successfully').format(obj=ontology))
            return response
    else:
        form = PrimaryFileForm(file_list=files)

    context = {
        'form': form,
        'title': _('Set Primary File'),
        'form_url': reverse('renderer:admin_edit_ontology_primary_file',
                            args=[pk,]),
    }
    return render(request, 'renderer/admin/add_update_object.html', context)

class ViewUsers(UserPassesTestMixin, ListView):
    model = get_user_model()
    template_name = 'renderer/admin/list_view.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Users')
        context['empty_text'] = _('You have no users to manage yet')
        context['edit_url'] = 'renderer:admin_edit_user'
        message = self.request.COOKIES.get('message')
        if message:
            context['messages'] = [message,]
        return context

    def get(request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        response.delete_cookie('message')
        return response

    def test_func(self):
        return is_staff_check(self.request.user)

@permission_required('auth.add_user')
def add_user(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            instance = form.instance
            form.save()
            content_type = ContentType.objects.get(app_label='auth',
                                                   model='user')
            LogEntry.objects.log_action(request.user.pk,
                                        content_type.pk,
                                        instance.pk,
                                        str(instance),
                                        ADDITION)
            response = redirect('renderer:admin_edit_user', pk=instance.pk)
            response.set_cookie('message',
                                _('{obj} has been added successfully').format(obj=instance))
            return response
    else:
        form = UserCreationForm()
    context = {
        'form': form,
        'title': _('Add User'),
        'form_url': reverse('renderer:admin_add_user'),
    }
    return render(request, 'renderer/admin/add_update_object.html', context)

@permission_required('auth.change_user')
def edit_user(request, pk):
    user = get_object_or_404(get_user_model(), id=pk)
    if request.method == 'POST':
        form = UserChangeForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            instance = form.instance
            content_type = ContentType.objects.get(app_label='auth',
                                                   model='user')
            LogEntry.objects.log_action(request.user.pk,
                                        content_type.pk,
                                        instance.pk,
                                        str(instance),
                                        CHANGE)
            response = redirect('renderer:admin_view_users')
            response.set_cookie('message',
                                _('{obj} has been updated successfully').format(obj=instance))
            return response
    else:
        form = UserChangeForm(instance=user)
    context = {
        'form': form,
        'title': _('Edit User'),
        'form_url': reverse('renderer:admin_edit_user', args=[pk,]),
        'delete_url': reverse_lazy('renderer:admin_delete_user',
                                   args=[pk]),
        'intermediary_crumb': { 'url': reverse('renderer:admin_view_users'),
                                'text': _('Users')}
    }
    message = request.COOKIES.get('message')
    if message:
        context['messages'] = [message,]
    response = render(request, 'renderer/admin/add_update_object.html', context)
    response.delete_cookie('message')
    return response

@permission_required('renderer:delete_user')
def delete_user(request, pk):
    user = get_object_or_404(get_user_model(), id=pk)
    if request.method == 'POST':
        user.delete()
        content_type = ContentType.objects.get(app_label='auth',
                                               model='user')
        LogEntry.objects.log_action(request.user.pk,
                                    content_type.pk,
                                    user.pk,
                                    str(user),
                                    DELETION)
        response = redirect('renderer:admin_view_users')
        response.set_cookie('message',
                            _('{obj} has been deleted successfully').format(obj=user))
        return response

    context = {
        'object': user,
        'title': _('Delete user'),
    }
    return render(request, 'renderer/admin/delete_confirmation.html', context)
