###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from django.urls import path
from . import admin_views, doc_views, views

app_name = 'renderer'

urlpatterns = [
    path('', views.index, name='index'),
    path('textbook/<int:pk>/', views.view_textbook,
         name='view_textbook'),
    path('admin/', admin_views.index, name='admin_index'),
    path('admin/add_textbook/', admin_views.add_textbook,
         name='admin_add_textbook'),
    path('admin/textbook/<int:pk>/', admin_views.edit_textbook,
         name='admin_edit_textbook'),
    path('admin/textbook/<int:pk>/delete', admin_views.delete_textbook,
         name='admin_delete_textbook'),
    path('admin/add_ontology/', admin_views.OntologyCreate.as_view(),
         name='admin_add_ontology'),
    path('admin/ontology/<int:pk>/', admin_views.OntologyUpdate.as_view(),
         name='admin_edit_ontology'),
    path('admin/ontology/<int:pk>/default-file',
         admin_views.set_ontology_primary_file,
         name='admin_edit_ontology_primary_file'),
    path('admin/ontology/<int:pk>/delete',
         admin_views.OntologyDelete.as_view(), name='admin_delete_ontology'),
    path('docs', doc_views.Index.as_view(), name='docs_help'),
    path('docs/admin', doc_views.Admin.as_view(), name='docs_admin'),
    path('docs/user', doc_views.User.as_view(), name='docs_user'),
    path('admin/users/', admin_views.ViewUsers.as_view(),
         name='admin_view_users'),
    path('admin/user/add/', admin_views.add_user,
         name='admin_add_user'),
    path('admin/user/<int:pk>/', admin_views.edit_user,
         name='admin_edit_user'),
    path('admin/user/<int:pk>/delete', admin_views.delete_user,
         name='admin_delete_user'),
]
