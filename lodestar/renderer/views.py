###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import os

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.utils.translation import gettext_lazy as _

from .models import Textbook

@login_required
def index(request):
    textbooks = Textbook.objects.all()
    context = {
        'textbooks': textbooks,
        'title': _('Textbooks'),
    }
    return render(request, 'renderer/index.html', context)

@login_required
def view_textbook(request, pk):
    textbook = get_object_or_404(Textbook, id=pk)
    textbook_file = os.path.join(textbook.source.html_directory, 'index.html')

    context = {
        'title': textbook,
        'embed_file': textbook_file,
    }
    if os.path.exists(textbook_file):
        return render(request, 'renderer/view_textbook.html', context)
    return render(request, 'renderer/textbook_pending.html', context)
