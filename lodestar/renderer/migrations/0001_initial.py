# Generated by Django 2.2.2 on 2019-07-01 12:45

from django.db import migrations, models
import django.db.models.deletion
import lodestar.renderer.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('sourceparser', '0002_auto_20190701_1245'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ontology',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='name')),
                ('source_file', models.FileField(upload_to=lodestar.renderer.models.Ontology.upload_source)),
                ('primary_file', models.FileField(upload_to=lodestar.renderer.models.Ontology.upload_primary)),
                ('md5sum', models.BinaryField(blank=True, max_length=128)),
                ('file_updated', models.BooleanField(blank=True, null=True)),
                ('namespace', models.CharField(max_length=200, verbose_name='namespace')),
                ('definition_preposition', models.CharField(max_length=200, verbose_name='defintion preposition')),
            ],
        ),
        migrations.CreateModel(
            name='Textbook',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('html_directory', models.FileField(upload_to='')),
                ('ontology', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='renderer.Ontology')),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sourceparser.Source')),
            ],
        ),
    ]
