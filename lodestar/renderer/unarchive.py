###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import mimetypes
import zipfile

def unzip(filename, destination):
    zip_ref = zipfile.ZipFile(filename, 'r')
    namelist = zip_ref.namelist()
    zip_ref.extractall(destination)
    zip_ref.close()
    return namelist

supported_archive_formats = {
    'application/zip': unzip,
}

def is_archive(filename):
    mimetype = mimetypes.guess_type(filename)[0]
    return mimetype in supported_archive_formats.keys()

def unarchive(filename, destination):
    """
    Unarchives a supported archive.

    Args:
        filename: The archive to uncompress
        destination: The directory to uncompress it to

    Returns:
        A list of files that were extracted
    """
    mimetype = mimetypes.guess_type(filename)[0]
    return supported_archive_formats[mimetype](filename, destination)
