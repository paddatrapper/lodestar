###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import Ontology
from .widgets import UpdateFileInput

class PrimaryFileForm(forms.Form):
    primary_file = forms.ChoiceField(label=_('Primary ontology file'))

    def __init__(self, *args, **kwargs):
        file_list = kwargs.pop('file_list')
        super().__init__(*args, **kwargs)
        self.fields['primary_file'].choices = file_list

class TextbookForm(forms.Form):
    name = forms.CharField(max_length=200, label=_('Name'))
    author = forms.CharField(max_length=200, label=_('Author'))
    source_file = forms.FileField(label=_('Textbook file'),
                                  required=False,
                                  widget=UpdateFileInput)
    ontology = forms.ModelChoiceField(Ontology.objects.all(),
                                      label=_('Ontology'))

    def __init__(self, *args, require_file=True, **kwargs):
        self.require_source = require_file
        super().__init__(*args, **kwargs)

    def clean_source_file(self):
        source_file = self.cleaned_data['source_file']
        if self.require_source and not source_file:
            raise ValidationError(_('This field is required'), code='invalid')
        return source_file

class TextbookUpdateForm(TextbookForm):
    reprocess_textbook = forms.BooleanField(required=False,
                                            label=_('Reprocess annotations'))
