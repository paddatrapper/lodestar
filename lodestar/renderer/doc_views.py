###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################

from mdpages.views import MdPageView

class Index(MdPageView):
    md_file = 'help.md'

    extra_context = {
        'title': 'Help Documentation',
    }

class Admin(MdPageView):
    md_file = 'admin.md'

    extra_context = {
        'title': 'Site Admin Documentation',
    }

class User(MdPageView):
    md_file = 'user.md'

    extra_context = {
        'title': 'User Documentation',
    }
