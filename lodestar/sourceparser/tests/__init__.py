###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from lodestar.sourceparser.models import Source

from django.core.files import File

def get_source_file():
    filename = 'testdata/DataMiningForTheMasses.pdf'
    fp = open(filename, 'rb')
    source = Source(name='Data Mining for the Masses',
                    author='Dr Matthew North',
                    source_file=File(fp))
    return filename, source

