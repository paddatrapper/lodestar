###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import hashlib
import os

from django.core.files import File
from django.db.models.signals import post_save
from django.test import TestCase

from lodestar.sourceparser.models import Source
from lodestar.sourceparser.signals import handle_calculate_hash, convert_source
from . import get_source_file

BUFFER_SIZE = 65536 # 64kb

def get_file_hash(filename):
    md5 = hashlib.md5()
    with open(filename, 'rb') as f:
        while True:
            data = f.read(BUFFER_SIZE)
            if not data:
                break;
            md5.update(data)
    return md5.digest()

class SourceTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.filename, cls.source = get_source_file()

    def test_create_signals(self):
        expected_hash = get_file_hash(self.filename)
        self.assertEqual(self.source.md5sum, b'')
        handle_calculate_hash(Source, self.source, None)
        self.assertEqual(self.source.md5sum, expected_hash)
        self.assertTrue(self.source.file_updated)

    def test_update_signals_no_change(self):
        post_save.disconnect(convert_source, Source)
        filename, source = get_source_file()
        source.save()
        source.author = "Other author"
        handle_calculate_hash(Source, source, None)
        self.assertFalse(source.file_updated)
        post_save.connect(convert_source, Source)
        os.remove(source.source_file.path)

    def test_update_signals_change(self):
        post_save.disconnect(convert_source, Source)
        filename, source = get_source_file()
        source.save()
        old_file = source.source_file.path
        filename = 'README.md' 
        fp = open(filename, 'rb')
        source.source_file = File(fp)
        handle_calculate_hash(Source, source, None)
        self.assertTrue(source.file_updated)
        post_save.connect(convert_source, Source)
        os.remove(old_file)
