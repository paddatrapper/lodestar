###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import os
import shutil

from django.core.files import File
from django.db.models.signals import post_save
from django.test import TestCase

from lodestar.sourceparser.models import Source
from lodestar.sourceparser.parsers import Parser, PDF
from . import get_source_file
from lodestar.sourceparser.signals import convert_source

class ParseParserTest(TestCase):
    def test_parser_not_implemented(self):
        self.assertRaises(NotImplementedError, Parser.parse, None)

class ParsePDFTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        post_save.disconnect(convert_source, Source)
        cls.filename, cls.source = get_source_file()
        cls.source.save()

    def test_html_creation(self):
        html_dir = PDF.parse(self.source)
        filename = self.source.md5sum.hex()
        self.assertTrue(os.path.isdir(html_dir))
        self.assertTrue(os.path.isfile(os.path.join(html_dir, 'content.html')))
        self.assertFalse(os.path.isfile(os.path.join(html_dir, filename)))
        self.assertFalse(os.path.isfile(os.path.join(html_dir, 'content.html_temp')))
        result_text = open(os.path.join(html_dir, 'content.html'), 'r').read()
        shutil.rmtree(html_dir)
        os.remove(self.source.source_file.path)
        post_save.connect(convert_source, Source)
        self.assertIn('A Global Text Project Book', result_text)
        self.assertNotIn('<!DOCTYPE html', result_text)
