###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import os
import shutil

from django.db.models.signals import pre_save, post_save, pre_delete
from django.dispatch import receiver

from lodestar import is_file_changed_check

from .models import Source
from . import parsers

@receiver(post_save, sender=Source)
def convert_source(sender, instance, using, **kwargs):
    if instance.file_updated:
        html = parsers.parse_source(instance)
        instance.html_directory = html
        instance.save()

@receiver(pre_save, sender=Source)
def handle_calculate_hash(sender, instance, using, **kwargs):
    updated, md5sum = is_file_changed_check(sender, instance)
    instance.file_updated = updated
    instance.md5sum = md5sum

@receiver(pre_delete, sender=Source)
def handle_delete_source(sender, instance, using, **kwargs):
    os.remove(instance.source_file.path)
    shutil.rmtree(instance.html_directory)
