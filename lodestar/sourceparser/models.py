###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import mimetypes
import os

from django.db import models
from django.utils.translation import gettext_lazy as _

from lodestar import get_file_hash

class Source(models.Model):
    def upload(instance, filename):
        file_hash = get_file_hash(instance.source_file.file).hex()
        filename, extension = os.path.splitext(filename)
        return os.path.join('textbooks', 'source', file_hash + extension)

    name = models.CharField(_('name'), max_length=200)
    author = models.CharField(_('author'), max_length=200)
    source_file = models.FileField(upload_to=upload, max_length=130)
    md5sum = models.BinaryField(max_length=128, blank=True)
    file_updated = models.BooleanField(blank=True, null=True)
    html_directory = models.FilePathField()

    def get_mimetype(self):
        mimetype = mimetypes.guess_type(self.source_file.url)[0]
        return mimetype
    get_mimetype.short_description = _('File type')

    def __str__(self):
        return '{} by {} (Source)'.format(self.name, self.author)
