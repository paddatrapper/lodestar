###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import re
import os
import shutil
import subprocess

from django.conf import settings

registry = {}

def register_parser(parser):
    registry[parser.mimetype] = parser

def parse_source(source):
    mimetype = source.get_mimetype()
    return registry[mimetype].parse(source)

class ParserMeta(type):
    def __new__(meta, name, bases, class_dict):
        cls = type.__new__(meta, name, bases, class_dict)
        register_parser(cls)
        return cls

class Parser(metaclass=ParserMeta):
    mimetype = 'text/x.none'
    replacements = {}

    def parse(source):
        raise NotImplementedError('Must be implemented in subclasses')

    def clean_up_file(source_file, dest_file):
        clean_up_file(source_file, dest_file, Parser.replacements)

    def clean_up_file(source_file, dest_file, replacements):
        with open(source_file, 'r') as source, open(dest_file, 'w') as dest:
            text = source.read()
            for key, value in replacements.items():
                text = re.sub(key, value, text)
            dest.write(text)

class PDF(Parser):
    mimetype = 'application/pdf'
    replacements = {
        r'<!DOCTYPE html PUBLIC "-\/\/W3C\/\/DTD XHTML 1.0 Transitional\/\/EN" "http:\/\/www.w3.org\/TR\/xhtml1\/DTD\/xhtml1-transitional.dtd"><html xmlns="http:\/\/www.w3.org\/1999\/xhtml">\n([\s\S]*)</html>': r'\1',
        r'<head>[\s\S]*</head>': r'',
        r'<body>([\s\S]*)</body>': r'\1',
        r'<pre>([\s\S]*)</pre>': r'\1',
        r'\n*m{0,4}(cm|cd|d?c{0,3})(xc|xl|l?x{0,3})(ix|iv|v?i{0,3})\n' : r'\n',
        # Assumes that textbooks will not have page numbers larger than 9999
        r'\n*[\d|\d\d|\d\d\d|\d\d\d\d]\n' : r'\n',
        r'+(.*)\n': r'<h1>\1</h1>\n',
        r'\n([A-Z][A-Z\s]*)\n': r'\n<h2>\1</h2>\n',
    }

    def parse(source):
        """
        Parses the PDF source file into a HTML page that can be manipulated
        and rendered.

        The process is:
        1. Copy PDF from source directory to the HTML directory
        2. Convert the PDF to HTML using pdftotext
        3. Delete the PDF copy in the HTML directory
        4. Clean up HTML file

        Args:
            source: The source PDF file

        Returns:
            The directory that contains the HTML version of the PDF

        Raises:
            OSError: There is an error with the pdftotext command
        """
        source_path = source.source_file.path
        filename = source.md5sum.hex()
        html_dir = os.path.join(settings.MEDIA_ROOT,
                                'textbooks',
                                'render',
                                source.md5sum.hex())
        filename = os.path.join(html_dir, filename)
        os.makedirs(html_dir, exist_ok=True)
        shutil.copyfile(source_path, filename)
        result = subprocess.run(args=['pdftotext', '-htmlmeta', filename],
                                cwd=html_dir,
                                capture_output=True)
        if result.returncode > 0:
            stderr = result.stderr.decode('utf-8')[:-1]
            raise OSError([result.returncode, stderr])
        content_file = os.path.join(html_dir, 'content.html')
        shutil.move(os.path.join(html_dir, source.md5sum.hex() + '.html'),
                    content_file + '_temp')
        os.remove(filename)
        PDF.clean_up_file(content_file + '_temp', content_file)
        os.remove(content_file + '_temp')
        return html_dir

    def clean_up_file(source_file, dest_file):
        Parser.clean_up_file(source_file, dest_file, PDF.replacements)

class HTML(Parser):
    mimetype = 'text/html'
    replacements = {}

    def parse(source):
        """
        Parses the PDF source file into a HTML page that can be manipulated
        and rendered.

        The process is:
        1. Copy PDF from source directory to the HTML directory
        2. Convert the PDF to HTML using pdftotext
        3. Delete the PDF copy in the HTML directory
        4. Clean up HTML file

        Args:
            source: The source HTML file

        Returns:
            The directory that contains the HTML render version
        """
        source_path = source.source_file.path
        filename = source.md5sum.hex()
        html_dir = os.path.join(settings.MEDIA_ROOT,
                                'textbooks',
                                'render',
                                source.md5sum.hex())
        filename = os.path.join(html_dir, 'content.html')
        os.makedirs(html_dir, exist_ok=True)
        HTML.clean_up_file(source_path, filename)
        return html_dir

    def clean_up_file(source_file, dest_file):
        shutil.copyfile(source_file, dest_file)
