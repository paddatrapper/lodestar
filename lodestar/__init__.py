###########################################################################
# Lodestar is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# Lodestar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Lodestar is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lodestar. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import hashlib

def get_file_hash(fileobj):
    md5 = hashlib.md5()
    for data in fileobj.chunks():
        md5.update(data)
    return md5.digest()

def is_file_changed_check(sender, instance):
    """
    Returns if the file has changed based on the stored md5sum in the database

    Args:
        sender: The instance's model class
        instance: The instance that is being changed. The file is assumed to
                  have been uploaded and stored in instance.source_file 
    Returns:
        A tuple where the first item indicates if the file has changed,
        and the second is the binary md5sum of the file.
    """
    md5sum = get_file_hash(instance.source_file.file)
    if instance.id:
        old_instance = sender.objects.get(id=instance.id)
        return (old_instance.md5sum != md5sum, md5sum)
    else:
        # New file
        return (True, md5sum)

