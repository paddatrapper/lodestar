#!/bin/bash

if [ -z $LODESTAR_ADMIN_PASSWORD ]; then
    LODESTAR_ADMIN_PASSWORD=admin
fi

if [ -z $LODESTAR_ADMIN_USER ]; then
    LODESTAR_ADMIN_USER=admin
fi

if [ -z $LODESTAR_ADMIN_EMAIL ]; then
    LODESTAR_ADMIN_EMAIL=admin@localhost
fi

echo Starting background process
WD=$(pwd)
cd /usr/src/app
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('$LODESTAR_ADMIN_PASSWORD', '$LODESTAR_ADMIN_EMAIL', '$LODESTAR_ADMIN_PASSWORD')" | python3 manage.py shell
python3 manage.py process_tasks &
cd $WD
echo Starting Nginx
nginx
echo Starting uWSGI
mkdir /var/run/uwsgi/
chown www-data:www-data /var/run/uwsgi
uwsgi --uid www-data --gid www-data /etc/uwsgi/lodestar.ini
