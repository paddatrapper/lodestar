# Lodestar

Lodestar is a digital smart textbook platform. It provides annotation of terms
within the textbook extracted from a related ontology.

# Building

Lodestar is built using Django. It requires Python 3.7, as it uses new features in
the subprocess module. A development environment can be set up using:

```
$ sudo apt install poppler-utils
$ virtualenv -p python3 pyenv
$ pyenv/bin/pip install -r requirements.txt
$ mkdir pyenv/nltk_data
$ pyenv/pin/python -m nltk.downloader wordnet punkt
$ pyenv/bin/python manage.py migrate
$ pyenv/bin/python manage.py createsuperuser
$ pyenv/bin/python manage.py runserver
```
This will start a development web server on http://127.0.0.1:8000/

Lodestar uses
[django-background-tasks](https://github.com/arteria/django-background-tasks)
to schedule the annotation of text after upload. Thus, in order to execute the
annotations, this needs to be run:

    pyenv/bin/python manage.py process_tasks

The execution of this command can be done through a systemd unit file. It will
check if there are any tasks to be run every few minutes and run them if
needed.

It further uses
[owlready2](https://owlready2.readthedocs.io/en/latest/index.html)
to interact with ontologies. This can be optimized by installing Cython within
the virtualenv:

```
$ pyenv/bin/pip install cython
$ pyenv/bin/pip install --force-reinstall --no-cache-dir owlready2
```

Generating translations is a two step process. First the `.po` file needs to be
generated from the code, then compiled into a format that GNU gettext can use.

```
$ cd lodestar
$ ../pyenv/bin/django-admin makemessages -l af
$ # edit the translation files if required
$ ../pyenv/bin/django-admin compilemessages -l af -f
```

Tests are run using

    pyenv/bin/python manage.py test

# Deployment

## Production

Deploying Lodestar in production requires some changes from the development
instance. These changes are documented in Mozilla's
[Django documentation](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Deployment)

When it comes to specifying production settings, use a `local_settings.py` file
that looks as follows:

```python
from settings import *

DEBUG = False
SECRET_KEY = <your secret key>
```

Any additional settings that you wish to override, can be included in this file.
In order to use this settings file use:

    export DJANGO_SETTINGS_MODULE=lodestar.local_settings

## Docker

There is a (sort-of) production-ready Docker image available
[here](https://hub.docker.com/r/bitcast/lodestar).
This is configurable using environment variables, though uses a sqlite3 database
as the backend. It creates a default user with the username and password
`admin`.

### Environment Variables

* `DJANGO_LOG_LEVEL` - the level of logs to emit to the console. Default: `INFO`
* `LODESTAR_SECRET_KEY` - the secret key to use in production. This should
always be set in production. It can be generated [here](https://www.miniwebtool.com/django-secret-key-generator/).
* `LODESTAR_DEBUG` - `False` for production. Default: `True`
* `LODESTAR_ADMIN_USER` - The default admin username. Default: None
* `LODESTAR_ADMIN_PASSWORD` - the default admin password. Default: None
* `LODESTAR_ADMIN_EMAIL` - the default admin email address. Default: None
